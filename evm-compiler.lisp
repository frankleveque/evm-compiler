

(defpackage :evm-compiler
  (:use :common-lisp))

(in-package :evm-compiler)
(load "opcodes.lisp")

(defparameter gas-cost 0)
(defconstant +verylow+ 3)
(defconstant +low+ 5)

(defun list2string (L)
  (reduce
   (lambda (a b)
     (concatenate 'string
                  (if (stringp a)
                      a
                      (write-to-string a))
                  (if (stringp b)
                      b
                      (write-to-string b))))
   L))

(defun flatten (l)
  (cond ((null l) nil)
        ((atom (car l)) (cons (car l) (flatten (cdr l))))
        (t (append (flatten (car l)) (flatten (cdr l))))))

;;------------------------------------------------------------------------------
(defun op-push1 (x)
  (incf gas-cost +verylow+)
  (append '(60) (list (format nil "~2,'0x" x))))

(defun op-push32 (x)
  (incf gas-cost +verylow+)
  (append '("7f") (list (format nil "~64,'0x" x))))


(defun op-mstore ()
  (incf gas-cost +verylow+)
  '("52"))

(defun op-add ()
  (format nil "0x~2,'0x" +ADD+))

;;------------------------------------------------------------------------------

#|
(defparameter out
  (list
   (op-push32 #xAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA)
   (op-push32 #xBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB)
   (op-push32 #xCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC)
   (op-push32 #xDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD)
   (op-push32 #xEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE)
   (op-push32 #xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)
   (op-push1 0)
   (op-mstore)
   ))


                                        ;(format t "~a len ~a~%" out (list-length out)) ;
                                        ;(format t "~a len ~a~%" outflat (list-length outflat)) ;
(write-line
 (list2string
  (mapcar
   (lambda (x)
     (format nil "~a" x ))
   (flatten out))))
|#

(defun main()

  (format t "~a ~a ~%" (quote op-add) (op-add))
  (finish-output nil)
  ;;(read)
)



;;(sb-ext:save-lisp-and-die "evm-compiler.exe" :executable T  :purify T  :toplevel #'main)
;;(ccl:save-application "evm-compiler.exe" :prepend-kernel T :purify T :toplevel-function #'main)

